<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;


class Anonnce extends Model
{
    use HasFactory, Notifiable;
    protected $table = 'annonces';
    protected $fillable = [
        'titre','description','categorie_id','user_id','prix_min','prix_max','date_limit',
        'photo','address_latitude','address_longitude'

    ];
}
