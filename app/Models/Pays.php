<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Pays extends Model
{
    use HasFactory, Notifiable;
    protected $table = 'pays';
    protected $fillable = [
        'nom'
       
    ];
    public function Ville(){
        return $this->belongsTo(Ville::class,'pays_id','id');
    }
}
