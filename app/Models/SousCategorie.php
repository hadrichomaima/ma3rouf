<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class SousCategorie extends Model
{
    use HasFactory, Notifiable;
    protected $table = 'souscategories';
    protected $fillable = [
        'categorie_id','name','description','montant','photo'

    ];
    public function categorie()
    {
        return $this->belongsTo(Categorie::class);
    }

}
