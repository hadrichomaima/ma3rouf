<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
class Offre extends Model
{

    use HasFactory, Notifiable;
    protected $table = 'offres';
    protected $fillable = [
        'min_budget','max_budget','date_delivery','categorie_id','user_id','annonce_id'

    ];
    public function user()
    {
    return $this->belongsTo('App\Models\User');
    }
    public function souscategorie()
    {
    return $this->hasOne('App\Models\SousCategorie');
    }
    public function annonce()
    {
    return $this->hasOne('App\Models\Anonnce');
    }
}
