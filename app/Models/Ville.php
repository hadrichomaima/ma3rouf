<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Ville extends Model
{
    use HasFactory,Notifiable;
    protected $table = 'villes';
    protected $fillable = [
        'nom','pays_id',
       
    ];
public function Pays(){
    return $this->hasOne(Pays::class,'ville_id','id');
}
}
