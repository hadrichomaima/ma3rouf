<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;
class ForgotPasswordAPIController extends Controller
{
use SendsPasswordResetEmails;
public function sendResetLinkEmail(Request $request)
{
    $validator = Validator::make($request->all(), [
        'email' => ['required', 'email', 'max:255' ],
        ]);
        if ($validator->fails()) {
         return response()->json(['error' => $validator->errors()], 422);
        }
$response = $this->broker()->sendResetLink(
  $this->credentials($request));
return $response == Password::RESET_LINK_SENT
? $this->sendResetLinkResponse($request, $response)
: $this->sendResetLinkFailedResponse($request, $response);
}
protected function sendResetLinkResponse(Request $request, $response)
{
    
    // On success, a string $response is returned with value of RESET_LINK_SENT 
    // from the Password facade (the default is "passwords.sent") 
    // Laravel trans() function translates this response to the text  
    // designated in resources/lang/en/passwords.php

    return response()->json(['success' => ["message" => trans($response)] ], 200);   
}
/**
 * Send the response for a failed password reset link.
 *
 * @param  \Illuminate\Http\Request  $request
 * @param  string  $response
 * @return \Illuminate\Http\JsonResponse
 */
protected function sendResetLinkFailedResponse(Request $request, $response)
{        
    return response()->json(['error' => ["message" => trans($response)] ], 422);    
}
}
