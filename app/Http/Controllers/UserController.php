<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:user');
    }
    public function show()
    {
        return view('auth.login');
    }
    public function store(Request $request)
    {
        $user=new User();
        
        $user->email= $request->input('email');
        $user->password = Hash::make($request->input('password'));
        $user->confirmpassword = Hash::make($request->input('confirm password'));

        $user->save();
        return view('user');
    }
    public function settinguser(){
        
        return view('User.setting');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
}
