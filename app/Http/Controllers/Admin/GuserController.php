<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \Illuminate\Foundation\Auth\AuthenticatesUsers;

use App\Models\User;
class GuserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index()
    {
        $users=User::simplePaginate(3);
        return view('admin.userlist',['users'=>$users]); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user=User::find($id);
        $users=User::all();
        return view('admin.userlist',['user'=>$user,'users'=>$users]); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user=User::find($id);
        $user->name=$request->input('name');
        $user->prenom=$request->input('prenom');
        $user->adresse=$request->input('adresse');
        $user->email=$request->input('email');
        $user->password= $request->input('Password');
        $user->type_utilisateur= $request->input('typeutilisateur');

        $user->save();
        return redirect()->intended(route('admin.consult.user'));
        
    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     
        $user=user::find($id);
        $user->delete();
        return redirect()->intended(route('admin.consult.user'));
    }

public function Edituser($id)
{
   $user=User::find($id);
   return view('admin.Edituser',['users' =>$user]);
   
}
public function showProfile($id)
    {
        
        $users=User::find($id);
        return view('admin.consulterprofil',['users'=>$users]); 
    }

public function setting(){
    return view('admin.paramètres');
}
}
    


