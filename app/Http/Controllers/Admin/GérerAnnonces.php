<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use  App\Models\User;
use  App\Models\Anonnce;

class GérerAnnonces extends Controller
{
    public function ShowAnnonce()
    {
        $annonce=Anonnce::simplePaginate(3);

        return view('admin.Annonces.listeannonces',['annonces'=>$annonce]);
    }

    public function destroy($id)
    {
        $anonnce=Anonnce::find($id);
        $anonnce->delete();
        return redirect()->intended(route('admin.list.annonce'));
    }
}
