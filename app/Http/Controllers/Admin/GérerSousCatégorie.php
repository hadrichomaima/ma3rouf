<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Categorie;
use App\Models\SousCategorie;
use \Illuminate\Foundation\Auth\AuthenticatesUsers;
use File;


use Illuminate\Support\Facades\Storage;

use App\Models\User;
class GérerSousCatégorie extends Controller
{
    public function index()
    {
        $categorie=Categorie::all();
        return view('admin.sous_catégories.addsouscatégories',['categories'=>$categorie]);
    }
    public function list()
    {
        $souscategorie=SousCategorie::simplePaginate(3);
        return view('admin.sous_catégories.listsouscategorie',['categories'=>$souscategorie]);
    }
    public function store(Request $request)
    {
        $request->validate([


            'name' => ['required', 'string', 'unique:categories', 'max:255'],
            'montant' => ['required', 'string',  'max:255'],
            'file' => ['required'],
            'description' => ['required', 'string',  'max:255'],
            'categorie' => ['required'],
         ]);
        $cover=$request->file('file');
        $extension = $cover->getClientOriginalExtension();
        Storage::disk('categorie')->put($cover->getClientOriginalName(),  File::get($cover));

        $souscategorie=new SousCategorie();
        $souscategorie->name=$request->input('name');
        $souscategorie->montant=$request->input('montant');
        $souscategorie->categorie_id=$request->input('categorie');
        $souscategorie->photo=$cover->getClientOriginalName();
        $souscategorie->description=$request->input('description');
        $souscategorie->save();
        return redirect()->intended(route('admin.list.souscat'));
    }


    public function create()
    {
        //
    }
    public function update(Request $request, $id)
    {
        $cover=$request->file('file');
        $extension = $cover->getClientOriginalExtension();
        Storage::disk('categorie')->put($cover->getClientOriginalName(),  File::get($cover));

        $souscategorie=SousCategorie::find($id);
        $souscategorie->name=$request->input('name');
        $souscategorie->montant=$request->input('montant');
        $souscategorie->categorie_id=$request->input('categorie');
        $souscategorie->photo=$cover->getClientOriginalName();
        $souscategorie->description=$request->input('description');
        $souscategorie->save();
        return redirect()->intended(route('admin.list.souscat'));
    }


    public function destroy($id)
    {
        $scategorie=SousCategorie::find($id);
        $scategorie->delete();
        return redirect()->intended(route('admin.list.souscat'));
    }
    public function Editsouscategorie($id)
    {
        $souscategorie=SousCategorie::find($id);
        $a1=$souscategorie->categorie_id;
        // dd($a1);
        $aa=Categorie::all();
        $cat= Categorie::where('id', $a1)->pluck('name');
       
       return view('admin.sous_catégories.editsouscategorie',['categorie' =>$souscategorie,'categories'=>$cat]);

    }

}
