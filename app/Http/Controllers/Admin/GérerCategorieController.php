<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Categorie;
use Illuminate\Support\Facades\Input;
use File;

use Illuminate\Support\Facades\Storage;
class GérerCategorieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index()
    {
        return view('admin.categorie.addCat');
    }
    public function indexList()
    {
        $categorie=categorie::simplePaginate(3);
        return view('admin.categorie.listCat',['categories'=>$categorie]);

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([


            'name' => ['required', 'string', 'unique:categories', 'max:255'],
            'montant' => ['required', 'string',  'max:255'],
            'file' => ['required'],
            'description' => ['required', 'string',  'max:255'],
         ]);

        $cover=$request->file('file');
        $extension = $cover->getClientOriginalExtension();
        Storage::disk('categorie')->put($cover->getClientOriginalName(),  File::get($cover));

        $categorie=new Categorie();
        $categorie->name=$request->input('name');
        $categorie->montant=$request->input('montant');
        $categorie->photo=$cover->getClientOriginalName();
        $categorie->description=$request->input('description');
        $categorie->save();
        return redirect()->intended(route('admin.list.categorie'));


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {



        $categorie=Categorie::find($id);
        $categorie->name=$request->input('name');
        $categorie->montant=$request->input('montant');
        $categorie->description=$request->input('description');
        if($request->has('file')) {
            $image = $request->file('file');
            $filename = $image->getClientOriginalName();
            $image->move(public_path('storage/assets/images/categorie'), $filename);
            $categorie->photo = $request->file('file')->getClientOriginalName();
        }
        $categorie->save();
        return redirect()->intended(route('admin.list.categorie'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $categorie=Categorie::find($id);
        $categorie->delete();
        return redirect()->intended(route('admin.list.categorie'));
    }
    public function Editcategorie($id)
{
    $categorie=Categorie::find($id);
   return view('admin.categorie.editCat',['categories' =>$categorie]);

}
}
