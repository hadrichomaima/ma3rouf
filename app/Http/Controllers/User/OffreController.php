<?php

namespace App\Http\Controllers\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Offre;

use App\Models\Anonnce;


class OffreController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:user');
    }
    public function index()
    {
        return view('User.Offre.addoffre');
    }
    public function store($id,Request $request)
    {
    $annonce =Anonnce::find($id);
    $offre = new Offre;
    $offre->min_budget=$request->input('Minbudget');
    $offre->max_budget=$request->input('Maxbudget');
    $offre->date_delivery=$request->input('date_limit');
    $offre->user_id=$request->user()->id;
    $offre->annonce_id=$annonce->id;
    $offre->save();
    return redirect()->intended(route('user.offre.list'));
    }

    public function show_list()

    {  
       
        $user = Auth::user(); 
      
        $offres= offre::where('user_id', $user->id)->first()::simplePaginate(3);
        

        return view('User.offre.listoffre',['offre'=>$offres]);
    }
    public function destroy($id)
    {

        $Offre=Offre::find($id);

        $Offre->delete();
        return redirect()->intended(route('user.offre.list'));
    }
}
