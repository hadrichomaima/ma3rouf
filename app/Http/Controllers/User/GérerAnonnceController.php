<?php

namespace App\Http\Controllers\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Categorie;
use App\Models\Anonnce;
use App\Models\SousCategorie;
use File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;

class GérerAnonnceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:user');
    }
    public function index()
    {
        $data['categories']=Categorie::all();
        return view('User.annoce.addannoce',$data);
    }

     public function getSouscat(Request $request)
        {
            $data['sousCategories']=SousCategorie::where("categorie_id",$request->categorie_id)
                        ->get(["name","id"]);
            return response()->json($data);
    }

    public function indexlist()
    {
        $user = Auth::user();
        $annonces = Anonnce::where('user_id', $user->id)->first()::simplePaginate(3);
        return view('User.annoce.listannonce',['annonce'=>$annonces]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cover=$request->file('file');
        $extension = $cover->getClientOriginalExtension();
        Storage::disk('annonce')->put($cover->getClientOriginalName(),  File::get($cover));
        $annonce=new Anonnce();
        $annonce->titre=$request->input('titre');
        $annonce->prix_min=$request->input('min');
        $annonce->prix_max=$request->input('max');
        $annonce->date_limit=$request->input('date');
        $annonce->categorie_id=$request->input('scategorie');
        $annonce->address_latitude=$request->input('latitude');
        $annonce->address_longitude=$request->input('longitude');
        $annonce->user_id=$request->user()->id;
        $annonce->description=$request->input('decription');
        $annonce->photo=$cover->getClientOriginalName();
        $annonce->save();
        return redirect()->intended(route('user.annonce.list'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    public function description($id)
 
    {
        $anonnce=Anonnce::find($id);

        return view ('User.annoce.Descriptionannonce',['annonces'=>$anonnce]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        // $cover=$request->file('file');
        // $extension = $cover->getClientOriginalExtension();
        // Storage::disk('annonce')->put($cover->getClientOriginalName(),  File::get($cover));
        $annonce=Anonnce::find($id);
        $annonce->titre=$request->input('titre');
        $annonce->prix_min=$request->input('min');
        $annonce->prix_max=$request->input('max');
        $annonce->date_limit=$request->input('date');
        $annonce->categorie_id=$request->input('scategorie');
        $annonce->address_latitude=$request->input('latitude');
        $annonce->address_longitude=$request->input('longitude');
        $annonce->user_id=$request->user()->id;
        $annonce->description=$request->input('decription');
        if($request->has('file')) {
            $image = $request->file('file');
            $filename = $image->getClientOriginalName();
            $image->move(public_path('storage/assets/annonce'), $filename);
            $annonce->photo = $request->file('file')->getClientOriginalName();
        }
        $annonce->save();
        return redirect()->intended(route('user.annonce.list'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $Anonnce=Anonnce::find($id);
        $Anonnce->delete();
        return redirect()->intended(route('user.annonce.list'));
    }
    public function Editannonce($id)
    {
        $catall=Categorie::all();
        $annonces=Anonnce::find($id);
        $scat = SousCategorie::where('id', $annonces->categorie_id)->first();

    //dd($scat);
        $cat =Categorie::where('id', $scat->categorie_id)->first();

       return view('User.annoce.editannonce',['annonce' =>$annonces,'scategories'=>$scat,'categories'=>$cat,'allcat'=>$catall]);

    }
}
