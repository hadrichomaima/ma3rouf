<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\Ville;
use App\Models\Pays;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use File;

use Illuminate\Support\Facades\Storage;



class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;
    public $data3;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    // protected function validator(array $data)
    // {
    //     return Validator::make($data, [
    //         'name' => ['required', 'string', 'max:255'],
    //         'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
    //         'password' => ['required', 'string', 'min:8', 'confirmed'],
    //     ]);

    //}

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    public function showbashborad(){
        return view('dashboardUser');
    }


    public function showRegisterForm(){

        $data1=Pays::all();

        foreach($data1 as $d1){
            $data2=$d1->id;
            $dd1=Ville::where('pays_id',$data2)->get();

        }
        // dd($dd1);

        // return view('auth.register',['pays'=>$data1,'ville'=>$dd1]);

        $data['pays'] = Pays::get(["nom","id"]);
        return view('auth.register',$data);

    }
    public function getVille(Request $request)
    {
        $data['villes'] = Ville::where("pays_id",$request->pays_id)
                    ->get(["nom","id"]);
        return response()->json($data);
    }

    protected function create(Request $request)
    {



        $request->validate([


            'name' => ['required', 'string',  'max:255'],
            'prenom' => ['required', 'string',  'max:255'],
            'file' => ['required',   'max:255'],
            'adresse' => ['required', 'string',  'max:255'],
            'ville' => ['required', 'max:255'],
            'pays' => ['required', 'max:255'],
            'radion_button' => ['required', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users','min:8'],
            'password' => ['required','nullable','required_with:password_confirmation','string','confirmed'],
            'password_confirmation' => ['required', 'string',  'max:255'],

         ]);
         $cover=$request->file('file');
         $extension = $cover->getClientOriginalExtension();
         Storage::disk('images')->put($cover->getClientOriginalName(),  File::get($cover));

        $user=new User();
        $user->name=$request->input('name');
        $user->prenom=$request->input('prenom');
        $user->photo=$cover->getClientOriginalName();
        $user->adresse=$request->input('adresse');
        $user->ville_id=$request->input('ville');
        $user->type_utilisateur = $request->input('radion_button',0);
        $user->email=$request->input('email');
        $user->password =Hash::make($request->input('password'));
        $user->save();




    return redirect()->intended(route('login'));}
}
