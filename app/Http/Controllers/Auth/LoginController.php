<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use Laravel\Socialite\Facades\Socialite;


class LoginController extends Controller
{
    use AuthenticatesUsers;
    public function __construct()
    {
        
           
            $this->middleware('guest:user',['except'=>['logout']]);
 
        
    }
    public function showLoginForm(){
        return view('auth.login_User');
    }
    public function Login(request $request){
        $request->validate([
            
           
            'email' => ['required','email',  'max:255'],
            'password' => ['required', 'string','min:4'],
            ]);
       if(Auth::guard('user')->attempt(['email'=>$request->email,'password'=>$request->password],$request->remember)){
          return redirect()->intended(route('profil'));
    }
    return back()->withInput($request->only('email', 'remember'))->withErrors(['email'=>'email or password is invalid']);
   }

   public function logout(){
  
    Auth::guard('user')->logout();
    return redirect('/user/login');
}
public function redirectToGoogle()
   {
       return Socialite::driver('google')->redirect();
   }
   public function handleGoogleCallback()
   {
       $user=Socialite::driver('google')->stateless()->user();
       return $user->name;
     
}
public function redirectToFacebook()
   {
       return Socialite::driver('facebook')->redirect();
   }
   public function handleFacebookCallback()
   {
       $user=Socialite::driver('facebook')->stateless()->user();
       return $user->name;
     
}
}

   