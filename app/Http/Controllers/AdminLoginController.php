<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \Illuminate\Foundation\Auth\AuthenticatesUsers;

use Auth;


class AdminLoginController extends Controller
{
    public function __construct()
    {
        
           
            $this->middleware('guest:admin',['except'=>['logout']]);
 
        
    }
    public function showAdminLoginForm(){
        return view('login_Admin');
    }
    public function Login(request $request){
        $request->validate([
            
            
       
        
        'email' => ['required','email'],
        'password' => ['required', 'string'],
        ]);
   if(Auth::guard('admin')->attempt(['email'=>$request->email,'password'=>$request->password],$request->remember)){
      return redirect()->intended(route('dashboard'));
}
return back()->withInput($request->only('email', 'remember'))->withErrors(['email'=>'email or password is invalid']);
        }
public function logout(){
  
    Auth::guard('admin')->logout();
    return redirect('/admin/login');
}

      
}
