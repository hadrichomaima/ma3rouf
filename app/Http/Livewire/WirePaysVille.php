<?php

namespace App\Http\Livewire;

use App\Models\Categorie;
use App\Models\SousCategorie;
use Livewire\Component;

class WirePaysVille extends Component
{

    public $souscategories;
    public $Selectedcategorie=null;
    public $Selectedsouscategorie=null;
    public function mount($Selectedsouscategorie=null)
   {
    $this->Selectedsouscategorie = $Selectedsouscategorie;
    if (!is_null($Selectedsouscategorie)) {
        $souscat = SousCategorie::all()->find($Selectedsouscategorie);
        if ( $souscat) {
            $this->souscategories = SousCategorie::where('categorie_id', $souscat->categorie_id)->get();

            $this->Selectedcategorie =$souscat->categorie_id;
        }
    }
    }

    public function render()
    {
        $categories=Categorie::all();
        return view('livewire.wire-pays-ville',['categories'=>$categories]);
    }

    public function updatedSelectedcategorie($categorie_id)
    {
        $this->souscategories = SousCategorie::where('categorie_id', $categorie_id)->get();

    }


}
