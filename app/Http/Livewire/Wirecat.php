<?php

namespace App\Http\Livewire;

use Livewire\Component;

use App\Models\Categorie;

class Wirecat extends Component
{   public $categories;
    public $Selectedcategorie=null;
    public function mount()
    {
        $this->categories=Categorie::all();

     }
    public function render()
    {
        return view('livewire.wirecat');

    }
    
}
