<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Ville;
use App\Models\Pays;

class villeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pays::create([
            'id' =>1,
            'nom' => "tunisie"
        ]);
        Ville::create([
            'nom' => 'Monastir',
            'pays_id' => '1'
        ]);
        Ville::create([
            'nom' => 'Ariana',
            'pays_id' => '1'
        ]);
        Ville::create([
            'nom' => 'Béja',
            'pays_id' => '1'
        ]);
        Ville::create([
            'nom' => 'Gafsa',
            'pays_id' => '1'
        ]);
        Ville::create([
            'nom' => 'Gabes',
            'pays_id' => '1'
        ]);
        Ville::create([
            'nom' => 'Jendouba',
            'pays_id' => '1'
        ]);
        Ville::create([
            'nom' => 'Ben Arous',
            'pays_id' => '1'
        ]);
        Ville::create([
            'nom' => 'Kairaoun',
            'pays_id' => '1'
        ]);
        Ville::create([
            'nom' => 'kasserine',
            'pays_id' => '1'
        ]);
        Ville::create([
            'nom' => 'kbeli',
            'pays_id' => '1'
        ]);
        Ville::create([
            'nom' => 'La Manouba',
            'pays_id' => '1'
        ]);
        Ville::create([
            'nom' => 'Le Kef',
            'pays_id' => '1'
        ]);
        Ville::create([
            'nom' => 'Mahdia',
            'pays_id' => '1'
        ]);
        Ville::create([
            'nom' => 'Médenine',
            'pays_id' => '1'
        ]);
        Ville::create([
            'nom' => 'Nabeul',
            'pays_id' => '1'
        ]);
        Ville::create([
            'nom' => 'Sidi Bouzid',
            'pays_id' => '1'
        ]);
        Ville::create([
            'nom' => 'Siliana',
            'pays_id' => '1'
        ]);
        Ville::create([
            'nom' => 'Tataouine',
            'pays_id' => '1'
        ]);
        Ville::create([
            'nom' => 'Tozeur',
            'pays_id' => '1'
        ]);
        Ville::create([
            'nom' => 'Tunis',
            'pays_id' => '1'
        ]);
        Ville::create([
            'nom' => 'Zaghaoune',
            'pays_id' => '1'
        ]);

    }
}
