
    <div>

        <div class="form-group row">
            <label for="categorie" class="col-md-4 col-form-label text-md-right">{{ __('categorie') }}</label>

            <div class="col-md-6">
                <select wire:model="Selectedcategorie" class="form-control">
                    <option value="" selected>choisir categorie</option>
                    @foreach($categories as $cat)
                        <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                    @endforeach
                </select>
            </div>

        </div>

        @if (!is_null($souscategories))
            <div class="form-group row">
                <label for="souscategorie" class="col-md-4 col-form-label text-md-right">{{ __('souscategorie') }}</label>

                <div class="col-md-6">
                    <select wire:model="Selectedsouscategorie" name="scategorie" class="form-control">
                        <option value="" selected>Choisir sous categorie</option>
                        @foreach($souscategories as $souscategorie)
                            <option value="{{ $souscategorie->id }}">{{ $souscategorie->name }}</option>
                        @endforeach
                    </select>
                </div>

            </div>
        @endif

</div>
