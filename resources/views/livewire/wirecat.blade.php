<div>


        <div class="col-md-8 ">
            <h5>Catégorie</h5>
            <select wire:model="Selectedcategorie" class="form-control">
                <option value="" selected>Choisir Categorie</option>
                @foreach($categories as $cat)
                    <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                @endforeach
            </select>
        </div>
        {{$Selectedcategorie}}
</div>
