@extends('layouts.app')
@section('content')

			<div class="login-register-page">
				<!-- Welcome Text -->
				

				<div class="welcome-text ">
					<h3 style="font-size: 26px;">S'inscrire</h3>
					<span>Trouver un nouveau service à essayer</span>
				</div>
				<form method="POST" action="{{ route('user.register') }}" enctype="multipart/form-data" >
				{{ csrf_field() }}

				<!-- Account Type -->

				<div class="row">
				<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">	</div>

				<div class="account-type ">
				<div class="form-check">
                <input class="account-type-radio " type="radio" name="radion_button" value="professionnel" id="flexRadioDefault1">


                 <label class="ripple-effect-dark" for="flexRadioDefault1"><i class="icon-material-outline-business-center"></i>
                 Professionnel
                 </label>
                </div>
                 <div class="form-check">
                 <input class="account-type-radio " type="radio" name="radion_button" value="personnel" id="flexRadioDefault2" checked>
                 <label class="ripple-effect-dark" for="flexRadioDefault2"><i class="icon-material-outline-business-center"></i>
                 Personnel
                 </label>
              </div>
			  @if ($errors->has('radion_button'))
						            <span class="notification error closeable">
                                        <strong>{{ $errors->first('radion_button') }}</strong>
										<a class="close" href="#"></a>
                                    </span>
                                @endif

				</div>
				</div>
				<div class="row">
				<div class="status-indicator col-6">
					<h3 style="font-size: 26px;  text-align:left; margin-bottom:5%; ">Informations Générale</h3>
				</div>
				<div class="status-indicator col-6">
					<h3 style="font-size: 26px;  text-align:left; margin-bottom:5%; ">Informations Du Compte</h3>
				</div>
				</div>
				<!-- Form -->
				<div class="row">
				<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">	</div>
					<div class="input-with-icon-left col-6">
						<i class="icon-material-outline-text-fields"></i>
						<input type="text" class="input-text with-border" name="name" id="emailaddress-register" placeholder="Nom" />
					    @if ($errors->has('name'))
						            <span class="notification error closeable">
                                        <strong>{{ $errors->first('name') }}</strong>
										<a class="close" href="#"></a>
                                    </span>
                                @endif



					</div>
					<div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}">	</div>
				<div class=" input-with-icon-left col-6">
					<div class="uploadButton " >

					<input class="uploadButton-input" type="file" name="file" accept="image/*" id="upload" />
					<label class="uploadButton-button ripple-effect" for="upload">Choisir image</label>
					<span class="uploadButton-file-name">Maximum file size: 10 MB</span>

					@if ($errors->has('file'))
					<span class="notification error closeable">
                    <strong>{{ $errors->first('file') }}</strong>
					<a class="close" href="#"></a>
                                    </span>
                                @endif
										</div>

					</div>
					</div>


					<div class="row">
					<div class="form-group{{ $errors->has('prenom') ? ' has-error' : '' }}"></div>
                    <div class="input-with-icon-left col-6">
						<i class="icon-material-outline-text-fields"></i>
						<input type="text" class="input-text with-border" name="prenom" id="emailaddress-register" placeholder="Prénom" />
						@if ($errors->has('prenom'))
						            <span class="notification error closeable">
                                        <strong>{{ $errors->first('prenom') }}</strong>
										<a class="close" href="#"></a>
                                    </span>
                                @endif
					</div>

					<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">	</div>
                    <div class="input-with-icon-left col-6" >
						<i class="icon-material-baseline-mail-outline"></i>
						<input type="text" class="input-text with-border" name="email" id="emailaddress-register" placeholder="Email"/>
						@if ($errors->has('email'))
						            <span class="notification error closeable">
                                        <strong>{{ $errors->first('email') }}</strong>
										<a class="close" href="#"></a>
                                    </span>
                                @endif
					</div>


					</div>
					<div class="row">
					<div class="form-group{{ $errors->has('adresse') ? ' has-error' : '' }}"></div>
                    <div class="input-with-icon-left  col-6">
						<i class="icon-material-outline-home"></i>
						<input type="text" class="input-text with-border" name="adresse" id="emailaddress-register" placeholder="Adresse"/>
						@if ($errors->has('adresse'))
						            <span class="notification error closeable">
                                        <strong>{{ $errors->first('adresse') }}</strong>
										<a class="close" href="#"></a>
                                    </span>
                                @endif
					</div>
					<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">	</div>
					<div class="input-with-icon-left col-6" title="Should be at least 8 characters long" data-tippy-placement="bottom">
						<i class="icon-material-outline-lock"></i>
						<input type="password" class="input-text with-border" name="password" id="password-register" placeholder="Mot de passe" />
						@if ($errors->has('password'))
                                    <span class="notification error closeable">
                                        <strong>{{ $errors->first('password') }}</strong>
										<a class="close" href="#"></a>
                                    </span>
                                @endif

					</div>

					</div>



					<div class="row">

					<div class="form-group{{ $errors->has('pays') ? ' has-error' : '' }}">		</div>
					<div class="input-with-icon-left col-3">
					<select class="selectpicker default" name="pays" id="pays-dropdown"  title="Pays" >

					@foreach($pays as $p)


					<option value="{{$p->id}}">{{$p->nom}}</option>
					@endforeach

					</select>

					 @if ($errors->has('pays'))
						            <span class="notification error closeable">
                                        <strong>{{ $errors->first('pays') }}</strong>
										<a class="close" href="#"></a>
                                    </span>
                    @endif

				</div>
					<div class="form-group{{ $errors->has('ville') ? ' has-error' : '' }}">		</div>
					<div class="input-with-icon-left col-3">
					<div class="sidebar-widget">


<select class="form-control" name="ville" id="ville-dropdown">
</select>

					@if ($errors->has('ville'))
						            <span class="notification error closeable">
                                        <strong>{{ $errors->first('ville') }}</strong>
										<a class="close" href="#"></a>
                                    </span>
                    @endif
				</div>
				</div>
				<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">	</div>
                    <div class="input-with-icon-left  col-6">
						<i class="icon-material-outline-lock"></i>
						<input type="password" class="input-text with-border" name="password_confirmation" id="password-repeat-register" placeholder="Confirme mot de passe" />
						@if ($errors->has('password_confirmation'))
                                    <span class="notification error closeable">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
										<a class="close" href="#"></a>
                                    </span>
                                @endif
					</div>
				</div>


			    <div class="row">


					</div>

					<button class="button full-width button-sliding-icon ripple-effect margin-top-10" type="submit" >Register <i cla
					ss="icon-material-outline-arrow-right-alt"></i></button>
					<div class="welcome-text ">
					<span>Vous avez déjà un compte ?  <a href="{{route('login')}}">Se connecter</a></span>
					</div>
				</form>

				<!-- Button -->

				<!-- Social Login -->
				<div class="social-login-separator"><span>or</span></div>
				<div class="social-login-buttons">
					<button class="facebook-login ripple-effect"><i class="icon-brand-facebook-f"></i> Register via Facebook</button>
					<button class="google-login ripple-effect"><i class="icon-brand-google-plus-g"></i> Register via Google+</button>
				</div>
			</div>
<script type="text/javascript">
document.addEventListener('DOMContentLoaded', function () {
$('#pays-dropdown').on('change', function() {
var pays_id = this.value;
$("#ville-dropdown").html('');
$.ajax({
url:"{{url('get-ville-by-pays')}}",
type: "POST",
data: {
pays_id: pays_id,
_token: '{{csrf_token()}}'
},
dataType : 'json',
success: function(result){
$('#ville-dropdown').html('<option value="">Select ville</option>');
$.each(result.villes,function(key,value){
$("#ville-dropdown").append('<option value="'+value.nom+'">'+value.nom+'</option>');
});

}
});
});
});
</script>
@endsection