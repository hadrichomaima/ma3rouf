<!doctype html>
<html lang="en">
<head>


<!-- Basic Page Needs
================================================== -->
<title>Ma3rouf</title>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- CSS
================================================== -->
<link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/colors/blue.css')}}">

</head>
<body>

<!-- Wrapper -->
<div id="wrapper">

<!-- Header Container
================================================== -->
<header id="header-container" class="fullwidth">

	<!-- Header -->
	<div id="header">
		<div class="img-container">
			<br/>

			
			<!-- Left Side Content -->
			<div class="left-side">
				
				<!-- Logo -->
				<div id="logo">
					<a href="index.html"><img src="/assets/images/logo.png" alt=""></a>
				</div>
			<!-- Left Side Content / End -->


			<!-- Right Side Content / End -->
	
				<!--  User Notifications / End -->

				<!-- User Menu -->
				
<div class="clearfix"></div>
<!-- Header Container / End -->

<!-- Titlebar
================================================== -->


<!-- Page Content
================================================== -->
</br>
</br>
<div class="container">
	<div class="row">
		<div class="col-xl-5 offset-xl-3">


			<div class="login-register-page">
				<!-- Welcome Text -->
				<div class="welcome-text">
					<h3>Bienvenue!</h3>
					<span> Trouver un nouveau service à essayer </span>
				</div>
					
				<!-- Form -->
				<form method="post" id="login-form"action="{{ route('user.login.submit') }}" >
				@csrf <!-- {{ csrf_field() }} -->
				<div class=" form-group{{ $errors->has('email') ? ' has-error' : '' }}"></div>
					<div class="input-with-icon-left">
						<i class="icon-material-baseline-mail-outline"></i>
						<input type="text" class="input-text with-border" name="email" id="emailaddress" placeholder="Adresse e-mail " />
					    @if ($errors->has('email'))
						            <span class="notification error closeable">
                                        <strong>{{ $errors->first('email') }}</strong>
										<a class="close" href="#"></a>
                                    </span>
                                @endif
					</div>
					<div class=" form-group{{ $errors->has('password') ? ' has-error' : '' }}"></div>

					<div class="input-with-icon-left">
						<i class="icon-material-outline-lock"></i>
						<input type="password" class="input-text with-border" name="password" id="password" placeholder="Mot de passe " />
					    @if ($errors->has('password'))
						            <span class="notification error closeable">
                                        <strong>{{ $errors->first('password') }}</strong>
										<a class="close" href="#"></a>
                                    </span>
                                @endif
					</div>
					<a href="{{route('show.forgot')}}" class="forgot-password">Forgot Password?</a>
				
				
				<!-- Button -->
				<button class="button full-width button-sliding-icon ripple-effect margin-top-10" type="submit" form="login-form">Log In <i class="icon-material-outline-arrow-right-alt"></i></button>
				</form>
				<!-- Social Login -->
				<div class="social-login-separator"><span>or</span></div>
				<div class="social-login-buttons">
					<button class="facebook-login ripple-effect"><i class="icon-brand-facebook-f"></i> <a href="{{ url('user/auth/facebook') }}">Log In via Facebook</a></button>
					<button class="google-login ripple-effect"><i class="icon-brand-google-plus-g"></i><a href="{{route('login.google')}}"> Log In via Google+</a></button>
				</div>
			</div>
</br>
<div>
<span> Vous n'avez pas de compte?  <a href="{{route('user.register')}}">Sign Up!</a></span>
</div>
		</div>
	</div>
</div>


<!-- Spacer -->
<div class="margin-top-70"></div>
<!-- Spacer / End-->

<!-- Footer
================================================== -->


<script src="assets/js/jquery-3.3.1.min.js"></script>
<script src="assets/js/jquery-migrate-3.0.0.min.js"></script>
<script src="assets/js/mmenu.min.js"></script>
<script src="assets/js/tippy.all.min.js"></script>
<script src="assets/js/simplebar.min.js"></script>
<script src="assets/js/bootstrap-slider.min.js"></script>
<script src="assets/js/bootstrap-select.min.js"></script>
<script src="assets/js/snackbar.js"></script>
<script src="assets/js/clipboard.min.js"></script>
<script src="assets/js/counterup.min.js"></script>
<script src="assets/js/magnific-popup.min.js"></script>
<script src="assets/js/slick.min.js"></script>
<script src="assets/js/custom.js"></script>

<!-- Snackbar // documentation: https://www.polonel.com/snackbar/ -->
<script>
// Snackbar for user status switcher
$('#snackbar-user-status label').click(function() { 
	Snackbar.show({
		text: 'Your status has been changed!',
		pos: 'bottom-center',
		showAction: false,
		actionText: "Dismiss",
		duration: 3000,
		textColor: '#fff',
		backgroundColor: '#383838'
	}); 
}); 
</script>

</body>
</html>