@extends('layouts.appUser')
@section('content')
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

<div class="dashboard-content-container" data-simplebar>
    <div class="dashboard-content-inner" >

        <!-- Row -->
        <div class="row">

            <!-- Dashboard Box -->
            <div class="col-xl-12">
                <div class="dashboard-box margin-top-0">

                    <!-- Headline -->
                    <div class="headline">
                        <h3><i class="icon-material-outline-business-center"></i> mes annonces</h3>
                    </div>

                    <div class="content">
@foreach($annonce as $ann)
                        <ul class="dashboard-box-list">
                            <li>
                                <!-- Job Listing -->
                                <div class="job-listing">

                                    <!-- Job Listing Details -->
                                    <div class="job-listing-details">

                                        <!-- Logo -->
<!-- 											<a href="#" class="job-listing-company-logo">
                                            <img src="images/company-logo-05.png" alt="">
                                        </a> -->

                                        <!-- Details -->
                                        <div class="job-listing-description">
                                            <h3 class="job-listing-title">{{$ann->titre}}<a href="#"></a> <span class="dashboard-status-button green">Pending Approval</span></h3>

                                            <!-- Job Listing Footer -->
                                            <div class="job-listing-footer">
                                                <ul>
                                                    <li><i class="icon-material-outline-date-range"></i> Publier le {{$ann->created_at->format('Y-m-d')}}</li>
                                                    <li><i class="icon-material-outline-date-range"></i> Expirer le {{$ann->date_limit}}</li>
                                                </ul>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <!-- Buttons -->
                                <div class="buttons-to-right always-visible">
                                <a href="{{url('user/description/'.$ann->id)}}" class="button ripple-effect" title="Voir description"><i class="feather feather-book"></i> Voir description </a>
                                

                                    <a href="{{url('user/editAnnonce/'.$ann->id)}}" class="button gray ripple-effect ico" title="Edit" data-tippy-placement="top"><i class="icon-feather-edit"></i></a>
                                    <a href="{{url('user/deleteAnnonce/'.$ann->id)}}" class="button gray ripple-effect ico" title="Remove" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></a>
                                </div>

                            </li>



                        </ul>
@endforeach
                    </div>
                </div>
            </div>

        </div>
        <!-- Row / End -->
{{ $annonce->links() }}
        <!-- Footer -->
        <div class="dashboard-footer-spacer"></div>
        <div class="small-footer margin-top-15">
            <div class="small-footer-copyrights">
                © 2018 <strong>Hireo</strong>. All Rights Reserved.
            </div>
            <ul class="footer-social-links">
                <li>
                    <a href="#" title="Facebook" data-tippy-placement="top">
                        <i class="icon-brand-facebook-f"></i>
                    </a>
                </li>
                <li>
                    <a href="#" title="Twitter" data-tippy-placement="top">
                        <i class="icon-brand-twitter"></i>
                    </a>
                </li>
                <li>
                    <a href="#" title="Google Plus" data-tippy-placement="top">
                        <i class="icon-brand-google-plus-g"></i>
                    </a>
                </li>
                <li>
                    <a href="#" title="LinkedIn" data-tippy-placement="top">
                        <i class="icon-brand-linkedin-in"></i>
                    </a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <!-- Footer / End -->

    </div>
</div>
<!-- Dashboard Content / End -->

</div>
<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function () {
    $('#categorie-dropdown').on('change', function() {
    var categorie_id = this.value;
    $("#souscat-dropdown").html('');
    $.ajax({
    url:"{{url('/user/get-souscat-by-cate')}}",
    type: "POST",
    data: {
    categorie_id: categorie_id,
    _token: '{{csrf_token()}}'
    },
    dataType : 'json',
    success: function(result){
    $('#souscat-dropdown').html('<option value="">select souscatégorie </option>');
    $.each(result.sousCategories,function(key,value){
    $("#souscat-dropdown").append('<option value="'+value.id+'">'+value.name+'</option>');
});

}
});
});
});
</script>

@endsection

