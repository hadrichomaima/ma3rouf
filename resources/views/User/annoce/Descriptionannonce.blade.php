@extends('layouts.app')
@section('content')


<div class="single-page-header" data-background-image="images/single-job.jpg">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="single-page-header-inner">
					<div class="left-side">
                    <div class="job-listing-description">

					<h3 class="job-listing-title">{{$annonces->titre}}<a href="#"></a> <span class="dashboard-status-button green"></span></h3>

                                            <!-- Job Listing Footer -->
                                            <div class="job-listing-footer">
                                                <ul>
                                                    <li><i class="icon-material-outline-date-range"></i>Publier le  {{$annonces->created_at->format('Y-m-d')}} </li>
                                                    <li><i class="icon-material-outline-date-range"></i>Expirer le {{$annonces->date_limit}}</li>
                                                </ul>
                                            </div>
                                        </div>

					</div>
					<div class="right-side">
						<div class="salary-box">
							<div class="salary-type">Budget minimum_maximum</div>
							
							<div>
							<ul>
                                                    <li><i  class="salary-type"></i> {{$annonces-> prix_min}} _  {{$annonces-> prix_max}}</li>
                                                    
												  </div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	
</div>

<!-- Page Content
================================================== -->
<div class="container">
	<div class="row">
		
		<!-- Content -->
		<div class="col-xl-8 col-lg-8 content-right-offset">

			<div class="single-page-section">
				<h3 class="margin-bottom-25">Description annonce </h3>
				<p>{{$annonces->titre}}</p>
                <p> {{$annonces->description}}</p>

			</div>

			<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
  Proposer un offre
</button>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <form  method="POST" action="{{url('user/offre/'.$annonces->id)}}"  >
{{ csrf_field() }}
<div class="mb-3">
    <label for="exampleInputEmail1" class="form-label">Min budget</label>
    <input type="text" class="form-control" name="Minbudget" >
    <div id="emailHelp" class="form-text">Fixez votre taux minimum .</div>
  </div>
  <div class="mb-3">
  <label for="exampleInputEmail1" class="form-label">Max budget</label>
    <input type="text" class="form-control" name="Maxbudget"  >
    <div id="emailHelp" class="form-text">Fixez votre taux Maximum .</div>
  </div>

   <div>
   <span  type="text" class="bidding-detail margin-top-30">Définissez votre <strong>délai de livraison</strong></span>
   </br>
   
   <div class="input-group mb-3" >
   <input type="text"  class="form-control"  value="1" name="date_limit"/>
							
   <select class="selectpicker default">
										<option selected>Jours</option>
										<option>Heures</option>
									</select>
							                
  </div>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
    </div>
    </form>
    </div>
  </div>
</div>
@endsection
