
@extends('layouts.appUser')
@section('content')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<div class="dashboard-content-container" data-simplebar>
    <div class="dashboard-content-inner" >

        <!-- Dashboard Headline -->

        <form method="POST" action="{{url('/user/updateAnnonce/'.$annonce->id)}}" enctype="multipart/form-data" >
			{{ csrf_field() }}

        <!-- Row -->
        <div class="row">

            <!-- Dashboard Box -->
            <div class="col-xl-12">
                <div class="dashboard-box margin-top-0">

                    <!-- Headline -->
                    <div class="headline">
                        <h3><i class="icon-feather-folder-plus"></i> Modifier annonce</h3>
                    </div>

                    <div class="content with-padding padding-bottom-10">
                        <div class="row">

                            <div class="col-xl-4">
                                <div class="submit-field">
                                    <h5>Titre</h5>
                                    <input type="text" name="titre" class="with-border" placeholder="Titre" value="{{  $annonce->titre ? : old('titre')}}">
                                </div>
                            </div>


                                @livewire('App\Http\Livewire\WirePaysVille',['Selectedsouscategorie' => 1])


                            <div class="col-xl-6">
                                <div class="submit-field">
                                    <h5>Quel est votre budget estimé?</h5>
                                    <div class="row">
                                        <div class="col-xl-6">
                                            <div class="input-with-icon">
                                                <input class="with-border" name="min" type="text" placeholder="Minimum" value="{{$annonce->prix_min}}">
                                                <i class="currency">DT</i>
                                            </div>
                                        </div>
                                        <div class="col-xl-6">
                                            <div class="input-with-icon">
                                                <input class="with-border" name="max" type="text" placeholder="Maximum" value="{{$annonce->prix_max}}">
                                                <i class="currency">DT</i>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="col-xl-6">
                                <div class="submit-field">
                                    <h5>Date limite </h5>
                                    <div class="keywords-container">
                                        <div class="keyword-input-container">
                                            <input type="date" name="date" class="keyword-input with-border form-control" placeholder="Date limite" value="{{$annonce->date_limit}}"/>

                                        </div>
                                        <div class="keywords-list"><!-- keywords go here --></div>
                                        <div class="clearfix"></div>
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                            <div class="col-xl-5">

                                <div class="uploadButton margin-top-30">
                                        <input style="width:20%;" class="uploadButton-input" type="file" name="file" accept="image/*, application/pdf" id="upload" value="{{$annonce->photo}}"/>
                                        <label  class="uploadButton-button ripple-effect" for="upload">Image selectionné</label>

                                    </div>

                            </div>
                            <div class="col-xl-4">


                              </div>
                                    <div class="col-xl-3">
                                        <div class="input-with-icon margin-top-30">
                                            <button type="button"  data-toggle="modal" data-target="#exampleModal" class="btn" >Voir image de l'annonce </button>

                                        </div>

                                      </div>

                            </div>
                            <div >


                                <input type="hidden" name="latitude" id="latitude" class="form-control" />
                                <input type="hidden" name="longitude" id="longitude"  class="form-control" />
                            </div>
                            <div id="map" style="height: 500px;width: 1000px;"></div>
                            <div class="col-xl-12">
                                <div class="submit-field">
                                    <h5>Description</h5>
                                    <textarea cols="30" rows="5" name="decription" class="with-border" placeholder="Description" >{{  $annonce->description}}</textarea>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-12">
				<button class="button ripple-effect big margin-top-30" type="submit" >Ajouter  <i cla
					ss="icon-material-outline-arrow-right-alt"></i></button>
					</div>

        </div>
        <!-- Row / End -->
    </form>
        <!-- Footer -->
        <div class="dashboard-footer-spacer"></div>
        <div class="small-footer margin-top-15">
            <div class="small-footer-copyrights">
                © 2018 <strong>Hireo</strong>. All Rights Reserved.
            </div>
            <ul class="footer-social-links">
                <li>
                    <a href="#" title="Facebook" data-tippy-placement="top">
                        <i class="icon-brand-facebook-f"></i>
                    </a>
                </li>
                <li>
                    <a href="#" title="Twitter" data-tippy-placement="top">
                        <i class="icon-brand-twitter"></i>
                    </a>
                </li>
                <li>
                    <a href="#" title="Google Plus" data-tippy-placement="top">
                        <i class="icon-brand-google-plus-g"></i>
                    </a>
                </li>
                <li>
                    <a href="#" title="LinkedIn" data-tippy-placement="top">
                        <i class="icon-brand-linkedin-in"></i>
                    </a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <!-- Footer / End -->

    </div>
</div>
<!-- Dashboard Content / End -->

</div>

{{-- <script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function () {

    var categorie_id = $('#categorie-dropdown').val());
    $("#souscat-dropdown").html('');
    $.ajax({
    url:"{{url('/user/get-souscat-by-cate')}}",
    type: "POST",
    data: {
    categorie_id: categorie_id,
    _token: '{{csrf_token()}}'
    },
    dataType : 'json',
    success: function(result){
        $("#souscat-dropdown").append('<option value="'+{{$scategories->id}}+'" selected>'+{{$scategories->name}}+'</option>');
    $.each(result.sousCategories,function(key,value){
    $("#souscat-dropdown").append('<option value="'+value.id+'">'+value.name+'</option>');


});

}
});
});

</script> --}}

@section('scripts')
    @parent
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

    <script type="text/javascript">


$("#pac-input").focusin(function() {
            $(this).val('');
        });
        $('#latitude').val('');
        $('#longitude').val('');
        // This example adds a search box to a map, using the Google Place Autocomplete
        // feature. People can enter geographical searches. The search box will return a
        // pick list containing a mix of places and predicted search terms.
        // This example requires the Places library. Include the libraries=places
        // parameter when you first load the API. For example:
        // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
        function initAutocomplete() {
            var map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: 24.740691, lng: 46.6528521 },
                zoom: 16,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            // move pin and current location
            infoWindow = new google.maps.InfoWindow;
            geocoder = new google.maps.Geocoder();
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    map.setCenter(pos);
                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(pos),
                        map: map,
                        title: 'votre position  '
                    });
                    markers.push(marker);
                    marker.addListener('click', function() {
                        geocodeLatLng(geocoder, map, infoWindow,marker);
                    });
                    // to get current position address on load
                    google.maps.event.trigger(marker, 'click');
                }, function() {
                    handleLocationError(true, infoWindow, map.getCenter());
                });
            } else {
                // Browser doesn't support Geolocation
                console.log('dsdsdsdsddsd');
                handleLocationError(false, infoWindow, map.getCenter());
            }
            var geocoder = new google.maps.Geocoder();
            google.maps.event.addListener(map, 'click', function(event) {
                SelectedLatLng = event.latLng;
                geocoder.geocode({
                    'latLng': event.latLng
                }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            deleteMarkers();
                            addMarkerRunTime(event.latLng);
                            SelectedLocation = results[0].formatted_address;
                            console.log( results[0].formatted_address);
                            splitLatLng(String(event.latLng));
                            $("#pac-input").val(results[0].formatted_address);
                        }
                    }
                });
            });
            function geocodeLatLng(geocoder, map, infowindow,markerCurrent) {
                var latlng = {lat: markerCurrent.position.lat(), lng: markerCurrent.position.lng()};
                console.log(latlng);
                //$('#branch-latLng').val("("+markerCurrent.position.lat() +","+markerCurrent.position.lng()+")");
                $('#latitude').val(markerCurrent.position.lat());
                $('#longitude').val(markerCurrent.position.lng());
                geocoder.geocode({'location': latlng}, function(results, status) {
                    if (status === 'OK') {
                        if (results[0]) {
                            map.setZoom(8);
                            var marker = new google.maps.Marker({
                                position: latlng,
                                map: map
                            });
                            markers.push(marker);
                            infowindow.setContent(results[0].formatted_address);
                            SelectedLocation = results[0].formatted_address;
                            $("#pac-input").val(results[0].formatted_address);
                            infowindow.open(map, marker);
                        } else {
                            window.alert('No results found');
                        }
                    } else {
                        window.alert('Geocoder failed due to: ' + status);
                    }
                });
                SelectedLatLng =(markerCurrent.position.lat(),markerCurrent.position.lng());
            }
            function addMarkerRunTime(location) {
                var marker = new google.maps.Marker({
                    position: location,
                    map: map
                });
                markers.push(marker);
            }
            function setMapOnAll(map) {
                for (var i = 0; i < markers.length; i++) {
                    markers[i].setMap(map);
                }
            }
            function clearMarkers() {
                setMapOnAll(null);
            }
            function deleteMarkers() {
                clearMarkers();
                markers = [];
            }
            // Create the search box and link it to the UI element.
            var input = document.getElementById('pac-input');
            $("#pac-input").val("recherche ici ");
            var searchBox = new google.maps.places.SearchBox(input);
            map.controls[google.maps.ControlPosition.TOP_RIGHT].push(input);
            // Bias the SearchBox results towards current map's viewport.
            map.addListener('bounds_changed', function() {
                searchBox.setBounds(map.getBounds());
            });
            var markers = [];
            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function() {
                var places = searchBox.getPlaces();
                if (places.length == 0) {
                    return;
                }
                // Clear out the old markers.
                markers.forEach(function(marker) {
                    marker.setMap(null);
                });
                markers = [];
                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function(place) {
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }
                    var icon = {
                        url: place.icon,
                        size: new google.maps.Size(100, 100),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(25, 25)
                    };
                    // Create a marker for each place.
                    markers.push(new google.maps.Marker({
                        map: map,
                        icon: icon,
                        title: place.name,
                        position: place.geometry.location
                    }));
                    $('#latitude').val(place.geometry.location.lat());
                    console.log(place.geometry.location.lat());
                    $('#longitude').val(place.geometry.location.lng());
                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                map.fitBounds(bounds);
            });
        }
        function handleLocationError(browserHasGeolocation, infoWindow, pos) {
            infoWindow.setPosition(pos);
            infoWindow.setContent(browserHasGeolocation ?
                'Error: The Geolocation service failed.' :
                'Error: Your browser doesn\'t support geolocation.');
            infoWindow.open(map);
        }
        function splitLatLng(latLng){
            var newString = latLng.substring(0, latLng.length-1);
            var newString2 = newString.substring(1);
            var trainindIdArray = newString2.split(',');
            var lat = trainindIdArray[0];
            console.log(lat);
            var Lng  = trainindIdArray[1];
            console.log(Lng);
            $("#latitude").val(lat);
            $("#longitude").val(Lng);
        }



        a= $("#latitude");
        a2=$("#longitude");
        // $("#latitude").append('value="'+ a+'"');
        // $("#longitude").append('value="'+ a2+'"');

        console.log(a2);
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCe6mc2yN6t-g1PBBhqeMfDOQybpVsD2Wk&region=TN&libraries=places&callback=initAutocomplete&language=fr" async defer></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

@stop
<div style="padding-top: 15%;display: none;" class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">voir image</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <img src="{{asset('storage/assets/images/annonce/'.$annonce->photo)}}" alt=""/>
        </div>

      </div>
    </div>
  </div>
@endsection

