
@extends('layouts.appUser')
@section('content')

<div class="dashboard-content-container" data-simplebar>
    <div class="dashboard-content-inner" >

        <!-- Dashboard Headline -->

        <form method ="POST" action="{{route('user.pub.annonce')}}" enctype="multipart/form-data" >
			{{ csrf_field() }}
        <!-- Row -->
        <div class="row">

            <!-- Dashboard Box -->
            <div class="col-xl-12">
                <div class="dashboard-box margin-top-0">

                    <!-- Headline -->
                    <div class="headline">
                        <h3><i class="icon-feather-folder-plus"></i> Ajouter annonce</h3>
                    </div>

                    <div class="content with-padding padding-bottom-10">
                        <div class="row">

                            <div class="col-xl-4">
                                <div class="submit-field">
                                    <h5>Titre</h5>
                                    <input type="text" name="titre" class="with-border" placeholder="Titre">
                                </div>
                            </div>

                            <div class="col-xl-4">
                                <div class="submit-field">
                                    <h5>Catégorie</h5>
                                    <select class="selectpicker with-border" name="categorie" data-size="7" id="categorie-dropdown" title="Selection Catégorie">
                                        @foreach($categories as $categorie)
                                        <option value="{{$categorie->id}}">{{$categorie->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-xl-4">
                                <div class="submit-field">
                                    <h5>Sous-Catégorie</h5>
                                    <select class="form-control" name="scategorie"  id="souscat-dropdown">
                                    </select>
                                </div>
                            </div>

                            <div class="col-xl-4">
                                <div class="submit-field">
                                    <h5>positionnement</h5>
                                    <div class="input-with-icon">
                                        <div id="autocomplete-container">
                                            <input id="autocomplete-input" name="positionnement" class="with-border" type="text" placeholder="positionnement">
                                        </div>
                                        <i class="icon-material-outline-location-on"></i>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-6">
                                <div class="submit-field">
                                    <h5>Quel est votre budget estimé?</h5>
                                    <div class="row">
                                        <div class="col-xl-6">
                                            <div class="input-with-icon">
                                                <input class="with-border" name="min" type="text" placeholder="Minimum">
                                                <i class="currency">DT</i>
                                            </div>
                                        </div>
                                        <div class="col-xl-6">
                                            <div class="input-with-icon">
                                                <input class="with-border" name="max" type="text" placeholder="Maximum">
                                                <i class="currency">DT</i>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="col-xl-6">
                                <div class="submit-field">
                                    <h5>Date limite </h5>
                                    <div class="keywords-container">
                                        <div class="keyword-input-container">
                                            <input type="date" name="date" class="keyword-input with-border form-control" placeholder="Date limite"/>

                                        </div>
                                        <div class="keywords-list"><!-- keywords go here --></div>
                                        <div class="clearfix"></div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-xl-4">
                                <div class="submit-field">
                                <div class="uploadButton margin-top-30">
                                        <input class="uploadButton-input" type="file" name="file" accept="image/*, application/pdf" id="upload" />
                                        <label class="uploadButton-button ripple-effect" for="upload">Choisir image</label>
                                        <span class="uploadButton-file-name">Images de l'annonce'</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-12">
                                <div class="submit-field">
                                    <h5>Description</h5>
                                    <textarea cols="30" rows="5" name="decription" class="with-border" placeholder="Desciption"></textarea>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-12">
				<a href="{{url('user/listannonce/')}}"class="button ripple-effect big margin-top-30" type="submit" >Ajouter  <i cla
					ss="icon-material-outline-arrow-right-alt"></i></button>
					</div>

        </div>
        <!-- Row / End -->
    </form>
        <!-- Footer -->
        <div class="dashboard-footer-spacer"></div>
        <div class="small-footer margin-top-15">
            <div class="small-footer-copyrights">
                © 2018 <strong>Hireo</strong>. All Rights Reserved.
            </div>
            <ul class="footer-social-links">
                <li>
                    <a href="#" title="Facebook" data-tippy-placement="top">
                        <i class="icon-brand-facebook-f"></i>
                    </a>
                </li>
                <li>
                    <a href="#" title="Twitter" data-tippy-placement="top">
                        <i class="icon-brand-twitter"></i>
                    </a>
                </li>
                <li>
                    <a href="#" title="Google Plus" data-tippy-placement="top">
                        <i class="icon-brand-google-plus-g"></i>
                    </a>
                </li>
                <li>
                    <a href="#" title="LinkedIn" data-tippy-placement="top">
                        <i class="icon-brand-linkedin-in"></i>
                    </a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <!-- Footer / End -->

    </div>
</div>
<!-- Dashboard Content / End -->

</div>
<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function () {
    $('#categorie-dropdown').on('change', function() {
    var categorie_id = this.value;
    $("#souscat-dropdown").html('');
    $.ajax({
    url:"{{url('/user/get-souscat-by-cate')}}",
    type: "POST",
    data: {
    categorie_id: categorie_id,
    _token: '{{csrf_token()}}'
    },
    dataType : 'json',
    success: function(result){
    $('#souscat-dropdown').html('<option value="">select souscatégorie </option>');
    $.each(result.sousCategories,function(key,value){
    $("#souscat-dropdown").append('<option value="'+value.id+'">'+value.name+'</option>');
});

}
});
});
});
</script>

@endsection
