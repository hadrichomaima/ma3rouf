@extends('layouts.appUser')
@section('content')
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/colors/blue.css">

<!-- Row -->
<div class="dashboard-content-container" data-simplebar>
    <div class="dashboard-content-inner" >

        <!-- Row -->
        <div class="row">

            <!-- Dashboard Box -->
<div class="col-xl-12">
    <div class="dashboard-box margin-top-0">

        <!-- Headline -->
        <div class="headline">
            <h3><i class="icon-material-outline-supervisor-account"></i> Mes offres</h3>
       

        <div class="content">
@foreach($offre as $off)
            <ul class="dashboard-box-list">
                <li>
                    <!-- Overview -->
                    <div class="freelancer-overview manage-candidates">
                        <div class="freelancer-overview-inner">

                            <!-- Avatar -->
                            <div class="freelancer-avatar">
                                <div class="verified-badge"></div>
                                <a href="#"><img src="images/user-avatar-big-02.jpg" alt=""></a>
                            </div>
                            <!-- Name -->
                           
                            <div class="freelancer-name">
                                <h4><a href="#">Offre {{$off->id}}<img class="flag" src="images/flags/de.svg" alt="" title="Germany" data-tippy-placement="top"></a></h4>

                                <!-- Details -->
                                <span class="freelancer-detail-item"><a href="#"><i class="icon-feather-mail"></i> Annonce {{$off->annonce_id}}</a></span>

                                <!-- Rating -->
                                <div class="freelancer-rating">
                                    <div class="star-rating" data-rating="5.0"></div>
                                </div>
                   
                                <!-- Bid Details -->
                                <ul class="dashboard-task-info bid-info">
                                    <li><strong>{{$off->min_budget}} DT<strong><span>Prix fixe </span></li>
                                    <li><strong>{{$off->date_delivery}} Jours</strong><span>Délai de livraison</span></li>
                                </ul>

               
                           

                                <!-- Buttons -->
                                <div class="buttons-to-right always-visible margin-top-25 margin-bottom-0">
                                    <a href="#small-dialog-1"  class="popup-with-zoom-anim button ripple-effect" title="Accepter"><i class="icon-material-outline-check"></i> Accepter Offre</a>
                                    <a href="{{('/user/rejeteroffre/'.$off->id)}}"class="btn btn-danger" title="Rejeter"  ><i class="icon-feather-trash-2"></i> Rejeter offre</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>

            </ul>
            @endforeach
        </div>
    </div>
</div>

</div>
<!-- Row / End -->

<!-- Footer -->
{{ $offre->links() }}
<div class="dashboard-footer-spacer"></div>
<div class="small-footer margin-top-15">
<div class="small-footer-copyrights">
    © 2018 <strong>Hireo</strong>. All Rights Reserved.
</div>
<ul class="footer-social-links">
    <li>
        <a href="#" title="Facebook" data-tippy-placement="top">
            <i class="icon-brand-facebook-f"></i>
        </a>
    </li>
    <li>
        <a href="#" title="Twitter" data-tippy-placement="top">
            <i class="icon-brand-twitter"></i>
        </a>
    </li>
    <li>
        <a href="#" title="Google Plus" data-tippy-placement="top">
            <i class="icon-brand-google-plus-g"></i>
        </a>
    </li>
    <li>
        <a href="#" title="LinkedIn" data-tippy-placement="top">
            <i class="icon-brand-linkedin-in"></i>
        </a>
    </li>
</ul>
<div class="clearfix"></div>
</div>
<!-- Footer / End -->

</div>
</div>
<!-- Dashboard Content / End -->

</div>
<!-- Dashboard Container / End -->

</div>
<!-- Wrapper / End -->

@foreach($offre as $off)
<!-- Bid Acceptance Popup
================================================== -->
<div id="small-dialog-1" class="zoom-anim-dialog mfp-hide dialog-with-tabs">
<!--Tabs -->
<div class="sign-in-form">

<ul class="popup-tabs-nav">
<li><a href="#tab1">Accept Offer</a></li>
</ul>

<div class="popup-tabs-container">

<!-- Tab -->
<div class="popup-tab-content" id="tab">


<!-- Welcome Text -->
<div class="welcome-text">
    <h3>Accept Offer </h3>
    <div class="bid-acceptance margin-top-15">
    {{$off->min_budget}} DT
    </div>

</div>

<form id="terms">
    <div class="radio">
        <input id="radio-1" name="radio" type="radio" required>
        <label for="radio-1"><span class="radio-label"></span> J'ai lu et j'accepte les Termes et Conditions </label>
    </div>
</form>

<!-- Button -->
<button class="margin-top-15 button full-width button-sliding-icon ripple-effect" type="submit" form="terms">Accept <i class="icon-material-outline-arrow-right-alt"></i></button>

</div>
@endforeach
</div>
</div>
</div>
<!-- Bid Acceptance Popup / End -->


<!-- Send Direct Message Popup
================================================== -->
<div id="small-dialog-2" class="zoom-anim-dialog mfp-hide dialog-with-tabs">

<!--Tabs -->
<div class="sign-in-form">

<ul class="popup-tabs-nav">
<li><a href="#tab2">Send Message</a></li>
</ul>

<div class="popup-tabs-container">

<!-- Tab -->
<div class="popup-tab-content" id="tab2">

<!-- Welcome Text -->
<div class="welcome-text">
    <h3>Direct Message To David</h3>
</div>
    
<!-- Form -->
<form method="post" id="send-pm">
    <textarea name="textarea" cols="10" placeholder="Message" class="with-border" required></textarea>
</form>

<!-- Button -->
<button class="button full-width button-sliding-icon ripple-effect" type="submit" form="send-pm">Send <i class="icon-material-outline-arrow-right-alt"></i></button>

</div>

</div>
</div>
</div>
<!-- Send Direct Message Popup / End -->

<!-- Scripts
================================================== -->
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/jquery-migrate-3.0.0.min.js"></script>
<script src="js/mmenu.min.js"></script>
<script src="js/tippy.all.min.js"></script>
<script src="js/simplebar.min.js"></script>
<script src="js/bootstrap-slider.min.js"></script>
<script src="js/bootstrap-select.min.js"></script>
<script src="js/snackbar.js"></script>
<script src="js/clipboard.min.js"></script>
<script src="js/counterup.min.js"></script>
<script src="js/magnific-popup.min.js"></script>
<script src="js/slick.min.js"></script>
<script src="js/custom.js"></script>

<!-- Snackbar // documentation: https://www.polonel.com/snackbar/ -->
<script>
// Snackbar for user status switcher
$('#snackbar-user-status label').click(function() { 
Snackbar.show({
text: 'Your status has been changed!',
pos: 'bottom-center',
showAction: false,
actionText: "Dismiss",
duration: 3000,
textColor: '#fff',
backgroundColor: '#383838'
}); 
}); 
</script>

@endsection
