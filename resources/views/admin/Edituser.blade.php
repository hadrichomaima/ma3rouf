
@extends('layouts.appAdmin')
@section('content')


<!-- Dashboard Container -->

	<!-- Dashboard Sidebar / End -->


	<!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			<!-- Dashboard Headline -->
			
	
			<!-- Row -->
			<div class="row">

				<!-- Dashboard Box -->
				<div class="col-xl-12">
					<div class="dashboard-box margin-top-0">

						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-feather-folder-plus"></i>mettre à jour les informations de la page </h3>
						</div>
						<form action="{{url('/admin/update/'.$users->id)}}" method="post">
      {{ csrf_field() }}
						<div class="content with-padding padding-bottom-10">
							<div class="row">
								<div class="col-xl-4">
									<div class="submit-field">
										<h5>Nom</h5>
										<input type="text" name="name" class="with-border"placeholder="Nom">
									</div>
								</div>

								<div class="col-xl-4">
									<div class="submit-field">
										<h5>Prénom</h5>
                                        <input type="text" name="prenom" class="with-border"placeholder="Prénom">
									</div>
								</div>

                                <div class="col-xl-4">
									<div class="submit-field">
										<h5>Adresse</h5>
										<div class="input-with-icon">
											<div id="autocomplete-container">
												<input id="autocomplete-input" name="adresse" class="with-border" type="text" placeholder="Adresse">
											</div>
											<i class="icon-material-outline-location-on"></i>
										</div>
									</div>
								</div>

								<div class="col-xl-4">
									<div class="submit-field">
										<h5>Email</h5>
                                        <input type="text" name="email" class="with-border" placeholder="Email">
										
									</div>
								</div>

								

								<div class="col-xl-4">
									<div class="submit-field">
										<h5>Mot de passe </h5>
									    <input type="password" name="Password" class="with-border" placeholder="Nouveau mot de passe">
													
												</div>
											</div>
											
										

								<div class="col-xl-4">
									<div class="submit-field">
										<h5>Type utilisateur   <i class="help-icon" data-tippy-placement="right" title="Maximum of 10 tags"></i></h5>
                                        <select class="selectpicker default" name="typeutilisateur"  data-selected-text-format="count" data-size="7" title="type utilisateur">
                                        <option value="Personel">Personel</option>
									    <option value="Professionnel">Professionnel</option>
										</select>			
							
									</div>
								</div>
                                <!-- User Status Switcher -->
								<div class="status-switch" id="snackbar-user-etats">
									
									<label class="user-online current-rtats">Activer</label>
									<label class="user-invisible">Désactiver</label>
									<!-- Status Indicator -->
									<span class="status-indicator" aria-hidden="true"></span>
								</div>	
								</div>
					
				<div class="col-xl-12">
					<!-- <a href="#" class="button ripple-effect big margin-center" tupe="submit"><i class="icon-feather-plus"></i><i class="icon-feather-edit"></i> Modifier</a> -->
					<button class="button full-width button-sliding-icon ripple-effect margin-top-10" type="submit" >Modifier <i cla
					ss="icon-material-outline-arrow-right-alt"></i></button>

				</div>
          </form>
			</div>
			
			</div>
						</div>
					</div>
				</div>

			<!-- Row / End -->

			<!-- Footer -->
			<div class="dashboard-footer-spacer"></div>
			<div class="small-footer margin-top-15">
				<div class="small-footer-copyrights">
					© 2018 <strong>Hireo</strong>. All Rights Reserved.
				</div>
				<ul class="footer-social-links">
					<li>
						<a href="#" title="Facebook" data-tippy-placement="top">
							<i class="icon-brand-facebook-f"></i>
						</a>
					</li>
					<li>
						<a href="#" title="Twitter" data-tippy-placement="top">
							<i class="icon-brand-twitter"></i>
						</a>
					</li>
					<li>
						<a href="#" title="Google Plus" data-tippy-placement="top">
							<i class="icon-brand-google-plus-g"></i>
						</a>
					</li>
					<li>
						<a href="#" title="LinkedIn" data-tippy-placement="top">
							<i class="icon-brand-linkedin-in"></i>
						</a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<!-- Footer / End -->

		</div>
	</div>
	<!-- Dashboard Content / End -->

</div>
<!-- Dashboard Container / End -->

</div>
<!-- Wrapper / End -->

@endsection