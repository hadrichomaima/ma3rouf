

@extends('layouts.appAdmin')
@section('content')
<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >

			<!-- Dashboard Headline -->

			<form method="POST" action="{{url('/admin/updatecat/'.$categorie->id)}}" enctype="multipart/form-data" >
			{{ csrf_field() }}
			<!-- Row -->
			<div class="row">

				<!-- Dashboard Box -->
				<div class="col-xl-12">
					<div class="dashboard-box margin-top-0">

						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-feather-folder-plus"></i> Ajouter catégorie</h3>
						</div>

						<div class="content with-padding padding-bottom-10">
							<div class="row">
                                @livewire('App\Http\Livewire\Wirecat',['Selectedsouscategorie' => 1])
								<div class="col-xl-4">
									<div class="submit-field">
										<h5>Nom</h5>
										<input type="hiden" name="name" class="with-border" placeholder="Nom"  value="{{ $categorie->name}}" >
									</div>
								</div>



								<div class="col-xl-4">
									<div class="submit-field">
										<h5>Montant </h5>
										<div class="input-with-icon">
											<div id="autocomplete-container">
												<input  type="hiden" id="autocomplete-input" name="montant" class="with-border" placeholder="Montant" value="{{ $categorie->montant}}">
											</div>
											<i class="icon-line-awesome-money"></i>
										</div>
									</div>
								</div>

								<div class="col-xl-4">
									<div class="submit-field">
									<div class="uploadButton margin-top-30">
											<input class="uploadButton-input" type="file" name="file" accept="image/*, application/pdf" id="upload" />
											<label class="uploadButton-button ripple-effect" for="upload">Choisir image</label>
											<span class="uploadButton-file-name">Images de la catégorie</span>
										</div>
									</div>
								</div>



								<div class="col-xl-12">
									<div class="submit-field">
										<h5>Description</h5>
										<input type="hiden" cols="30" rows="5" name="description" class="with-border" placeholder="Description" value="{{ $categorie->description}}"></textarea>

									</div>
								</div>

							</div>
						</div>
					</div>
				</div>

				<div class="col-xl-12">
				<button class="button ripple-effect big margin-top-30" type="submit" >Modifier <i cla
					ss="icon-material-outline-arrow-right-alt"></i></button>
					</div>

			</div>
			</form>
			<!-- Row / End -->

			<!-- Footer -->
			<div class="dashboard-footer-spacer"></div>
			<div class="small-footer margin-top-15">
				<div class="small-footer-copyrights">
					© 2018 <strong>Hireo</strong>. All Rights Reserved.
				</div>
				<ul class="footer-social-links">
					<li>
						<a href="#" title="Facebook" data-tippy-placement="top">
							<i class="icon-brand-facebook-f"></i>
						</a>
					</li>
					<li>
						<a href="#" title="Twitter" data-tippy-placement="top">
							<i class="icon-brand-twitter"></i>
						</a>
					</li>
					<li>
						<a href="#" title="Google Plus" data-tippy-placement="top">
							<i class="icon-brand-google-plus-g"></i>
						</a>
					</li>
					<li>
						<a href="#" title="LinkedIn" data-tippy-placement="top">
							<i class="icon-brand-linkedin-in"></i>
						</a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<!-- Footer / End -->

		</div>
	</div>
	<!-- Dashboard Content / End -->

</div>

@endsection
