






@extends('layouts.appAdmin')
@section('content')


	<!-- Dashboard Sidebar / End -->


	<!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>Gérer les utilisateurs </h3>
			

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="dark">
				<ul>
						<li><a href="#">Page d'accueil </a></li>
						<li><a href="#">Utilisateurs</a></li>
						<li>Gérer les utilisateurs</li>
					</ul>
				</nav>
			</div>
	
			<!-- Row -->
			<div class="row">

				<!-- Dashboard Box -->
				<div class="col-xl-12">
					<div class="dashboard-box margin-top-0">

						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-material-outline-supervisor-account"></i> {{$users->count()}} utilisateur</h3>
						</div>

						<div class="content">
						
							<ul class="dashboard-box-list">
							@foreach($users as $user)
								<li>
									<!-- Overview -->
									<div class="freelancer-overview manage-candidates">
										<div class="freelancer-overview-inner">

											<!-- Avatar -->
											<div class="freelancer-avatar">
												<div class="verified-badge"></div>
												<img src="../storage/assets/images/register/{{$user->photo}}" alt="">
												
											</div>

											<!-- Name -->
											<div class="freelancer-name">
												<h4><a href="#">{{$user->name}} {{$user->prenom}}  <img class="flag" src="images/flags/au.svg" alt="" title="Australia" data-tippy-placement="top"></a></h4>

												<!-- Details -->
												<span class="freelancer-detail-item"><a href="#"><i class="icon-feather-mail"></i> {{$user->email}}</a></span>
												<span class="freelancer-detail-item"><i class="icon-feather-phone"></i> {{$user->type_utilisateur}}</span>

												<!-- Rating -->
												<div class="freelancer-rating">
													<div class="star-rating" data-rating="5.0"></div>
												</div>

												<!-- Buttons -->
												<div class="buttons-to-right always-visible margin-top-25 margin-bottom-5">
												<a href="{{url('/admin/userprofile/'.$user->id)}}"  class="button ripple-effect"><i class="icon-feather-user"></i> Voir profile </a>
													<a href="{{url('/admin/edituser/'.$user->id)}}" class="button ripple-effect" title="Update user"><i class="icon-feather-edit"></i> mettre à jour </a>
                                                    <a href="{{url('/admin/delete/'.$user->id)}}" class="button ripple-effect" title="Remove User" data-tippy-placement="top"><i class="icon-feather-trash-2"></i> Effacer </a>
												</div>
												
											</div>
										</div>
									</div>
								</li>

                            @endforeach

							</ul>
						</div>
					</div>
				</div>
				
			</div>
			<!-- Row / End -->
			{{ $users->links() }}
			<!-- Footer -->
			<div class="dashboard-footer-spacer"></div>
			<div class="small-footer margin-top-15">
				<div class="small-footer-copyrights">
					© 2018 <strong>Hireo</strong>. All Rights Reserved.
				</div>
				<ul class="footer-social-links">
					<li>
						<a href="#" title="Facebook" data-tippy-placement="top">
							<i class="icon-brand-facebook-f"></i>
						</a>
					</li>
					<li>
						<a href="#" title="Twitter" data-tippy-placement="top">
							<i class="icon-brand-twitter"></i>
						</a>
					</li>
					<li>
						<a href="#" title="Google Plus" data-tippy-placement="top">
							<i class="icon-brand-google-plus-g"></i>
						</a>
					</li>
					<li>
						<a href="#" title="LinkedIn" data-tippy-placement="top">
							<i class="icon-brand-linkedin-in"></i>
						</a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<!-- Footer / End -->

		</div>
	</div>
	<!-- Dashboard Content / End -->
@endsection
