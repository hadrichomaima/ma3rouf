

@extends('layouts.appAdmin')
@section('content')
<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >

			<!-- Dashboard Headline -->

			<form method="POST" action="{{url('/admin/updateCat/'.$categories->id)}}" enctype="multipart/form-data" >
			{{ csrf_field() }}
			<!-- Row -->
			<div clas1s="row">

				<!-- Dashboard Box -->
				<div class="col-xl-12">
					<div class="dashboard-box margin-top-0">

						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-feather-folder-plus"></i> Ajouter catégorie</h3>
						</div>

						<div class="content with-padding padding-bottom-10">
							<div class="row">

								<div class="col-xl-4">
									<div class="submit-field">
										<h5>Nom</h5>
                                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">		</div>
										<input type="text" name="name" class="with-border" placeholder="Nom" value="{{  $categories->name ? : old('name')}}">
                                        @if ($errors->has('name'))
						            <span class="notification error closeable">
                                        <strong>{{ $errors->first('name') }}</strong>
										<a class="close" href="#"></a>
                                    </span>
                    @endif
									</div>
								</div>



								<div class="col-xl-4">
									<div class="submit-field">
										<h5>Montant </h5>
										<div class="input-with-icon">
											<div id="autocomplete-container">
                                                <div class="form-group{{ $errors->has('montant') ? ' has-error' : '' }}">		</div>
												<input id="autocomplete-input" name="montant" class="with-border" type="text" placeholder="Montant" value="{{  $categories->montant ? : old('montant')}}">
                                                @if ($errors->has('montant'))
						            <span class="notification error closeable">
                                        <strong>{{ $errors->first('montant') }}</strong>
										<a class="close" href="#"></a>
                                    </span>
                    @endif
											</div>
											<i class="icon-line-awesome-money"></i>
										</div>
									</div>
								</div>

								<div class="col-xl-4">
									<div class="submit-field">
									<div class="uploadButton margin-top-30">
											<input class="uploadButton-input" type="file" name="file" accept="image/*, application/pdf" id="upload"  value="{{  $categories->photo ? : old('photo')}}"/>
											<label class="uploadButton-button ripple-effect" for="upload">Choisir image</label>

											<span class="uploadButton-file-name">Images de la catégorie</span>
										</div>
									</div>
								</div>



								<div class="col-xl-12">
									<div class="submit-field">
										<h5>Description</h5>
                                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">		</div>
										<textarea cols="30" rows="5" name="description" class="with-border" placeholder="Description">{{ $categories->description}}</textarea>
                                        @if ($errors->has('description'))
						            <span class="notification error closeable">
                                        <strong>{{ $errors->first('description') }}</strong>
										<a class="close" href="#"></a>
                                    </span>
                    @endif

									</div>
								</div>

							</div>
						</div>
					</div>
				</div>

				<div class="col-xl-12">
				<button class="button ripple-effect big margin-top-30" type="submit" >Modifier <i cla
					ss="icon-material-outline-arrow-right-alt"></i></button>
					</div>

			</div>
			</form>
			<!-- Row / End -->

			<!-- Footer -->
			<div class="dashboard-footer-spacer"></div>
			<div class="small-footer margin-top-15">
				<div class="small-footer-copyrights">
					© 2018 <strong>Hireo</strong>. All Rights Reserved.
				</div>
				<ul class="footer-social-links">
					<li>
						<a href="#" title="Facebook" data-tippy-placement="top">
							<i class="icon-brand-facebook-f"></i>
						</a>
					</li>
					<li>
						<a href="#" title="Twitter" data-tippy-placement="top">
							<i class="icon-brand-twitter"></i>
						</a>
					</li>
					<li>
						<a href="#" title="Google Plus" data-tippy-placement="top">
							<i class="icon-brand-google-plus-g"></i>
						</a>
					</li>
					<li>
						<a href="#" title="LinkedIn" data-tippy-placement="top">
							<i class="icon-brand-linkedin-in"></i>
						</a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<!-- Footer / End -->

		</div>
	</div>
	<!-- Dashboard Content / End -->

</div>

@endsection
