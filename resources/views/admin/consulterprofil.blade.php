@extends('layouts.appAdmin')
@section('content')
<div class="clearfix"></div>

<div class="single-page-header freelancer-header" data-background-image="images/single-freelancer.jpg">
	<div class="container">
		
			<div class="col-md-12">
				<div class="single-page-header-inner">
				
						<div class="header-image freelancer-avatar"><img src="../../storage/assets/images/register/{{$users->photo}}" alt=""></div>
						<div class="header-details">
							<h3>{{$users->name}} {{$users->prenom}}  <span>{{$users->email}}</span></h3>
							<ul>
								<li><div class="star-rating" data-rating="5.0"></div></li>
								<li><img class="flag" src="{{asset('../assets/images/flags/Tunisia.svg')}}" alt=""> {{$users->ville_id}}</li>
								<li><div class="verified-badge-with-title">{{$users->type_utilisateur}}</div></li>
							</ul>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>




<!-- Spacer -->

<!-- Spacer / End-->
@endsection