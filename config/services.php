<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],
    
        'google' => [
            'client_id' => '947067180011-g9m962diop1rmdfi404md8pk3fcr4oaa.apps.googleusercontent.com',
            'client_secret' => 'mtgsmDiJXGBBEwf1-YXoKPQh',
            'redirect' => 'http://127.0.0.1:8000/login/google/callback',
       
        ],
    'facebook' => [
        'client_id' => 'App id goes here',
        'client_secret' => 'Secret goes herre',
        'redirect' => 'http://localhost:8000/auth/facebook/callback',
    ]

];
