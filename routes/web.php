<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/offre',function(){
    return view('User.Offre.addoffre');

});




/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




//Route::get('/admin/dashboard','App\Http\Controllers\AdminController@index');



Auth::routes();
// ///register
// Route::get('/', 'App\Http\Controllers\Auth\RegisterController@showRegisterForm')->name('user.register');
// Route::post('/', 'App\Http\Controllers\Auth\RegisterController@create')->name('user.register');
// Route::post('get-ville-by-pays','App\Http\Controllers\Auth\RegisterController@getVille');

//Route::get('/register','App\Http\Controllers\Auth\RegisterController@index');
//Route::post('/register', 'App\Http\Controllers\Auth\RegisterController@createUser')->name('register');
//Route::get('/login','App\Http\Controllers\UserController@show');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
//login admin
Route::prefix('admin')->group(function(){

Route::get('/login', 'App\Http\Controllers\AdminLoginController@showAdminLoginForm')->name('admin.login');
Route::post('/login', 'App\Http\Controllers\AdminLoginController@Login')->name('admin.login.submit');
Route::get('/admindashboard', 'App\Http\Controllers\DashboardController@show')->name('dashboard');
Route::post('/admindashboard', 'App\Http\Controllers\DashboardController@show')->name('dashboard');;
Route::get('/logout','App\Http\Controllers\AdminLoginController@logout')->name('admin.logout');
Route::get('/settings','App\Http\Controllers\Admin\GuserController@setting')->name('admin.settings');

Route::get('/consulteruser','App\Http\Controllers\Admin\GuserController@index')->name('admin.consult.user');
Route::get('userprofile/{id}','App\Http\Controllers\Admin\GuserController@showProfile')->name('admin.profile.user');
Route::get('/edituser/{id}','App\Http\Controllers\Admin\GuserController@Edituser');
Route::post('update/{id}','App\Http\Controllers\Admin\GuserController@update');
Route::get('delete/{id}','App\Http\Controllers\Admin\GuserController@destroy');

Route::get('/addsouscategorie','App\Http\Controllers\Admin\GérerSousCatégorie@index')->name('admin.add.souscat');
Route::post('/addsouscategorie','App\Http\Controllers\Admin\GérerSousCatégorie@store')->name('admin.store.souscat');
Route::get('/listsouscategorie','App\Http\Controllers\Admin\GérerSousCatégorie@list')->name('admin.list.souscat');
Route::get('/deletesousCat/{id}','App\Http\Controllers\Admin\GérerSousCatégorie@destroy');
Route::get('/editsousCat/{id}','App\Http\Controllers\Admin\GérerSousCatégorie@Editsouscategorie');
Route::post('updatecat/{id}','App\Http\Controllers\Admin\GérerSousCatégorie@update');
Route::get('/listannonces','App\Http\Controllers\Admin\GérerAnnonces@ShowAnnonce')->name('admin.list.annonce');
Route::get('/deleteAnnonce/{id}','App\Http\Controllers\Admin\GérerAnnonces@destroy');

});
 
//login user
Route::prefix('user')->group(function(){
Route::get('register', 'App\Http\Controllers\Auth\RegisterController@showRegisterForm')->name('user.register');
Route::post('register', 'App\Http\Controllers\Auth\RegisterController@create')->name('user.register');
Route::post('get-ville-by-pays','App\Http\Controllers\Auth\RegisterController@getVille');
Route::get('login', 'App\Http\Controllers\Auth\LoginController@showLoginForm')->name('user.login');
Route::post('login', 'App\Http\Controllers\Auth\LoginController@Login')->name('user.login.submit');
Route::get('profil', 'App\Http\Controllers\DashboardController@index')->name('profil');
Route::get('profil', 'App\Http\Controllers\DashboardController@index')->name('profil');
Route::get('/logout','App\Http\Controllers\Auth\LoginController@logout')->name('user.logout');
Route::post('/logout','App\Http\Controllers\Auth\LoginController@logout')->name('user.logout');
Route::get('/settings','App\Http\Controllers\UserController@settinguser')->name('user.setting');
Route::post('password_email', 'App\Http\Controllers\Auth\ForgotPasswordController@forgot')->name('password.forgot');
Route::get('forgot_password', 'App\Http\Controllers\Auth\ForgotPasswordController@ShowForgotPassword')->name('show.forgot');
//login with google+
Route::get('login/google', 'App\Http\Controllers\Auth\LoginController@redirectToGoogle')->name('login.google');
Route::get('login/google/callback', 'App\Http\Controllers\Auth\LoginController@handleGoogleCallback');

Route::get('auth/facebook', 'App\Http\Controllers\FbController@redirectToFacebook');
Route::get('auth/facebook/callback', 'App\Http\Controllers\FbController@facebookSignin');
///////annonce////////////////////////////
Route::get('annonce', 'App\Http\Controllers\User\GérerAnonnceController@index')->name('user.annonce');
Route::post('annonce', 'App\Http\Controllers\User\GérerAnonnceController@store')->name('user.pub.annonce');

Route::get('listannonce', 'App\Http\Controllers\User\GérerAnonnceController@indexlist')->name('user.annonce.list');
Route::post('get-souscat-by-cate','App\Http\Controllers\User\GérerAnonnceController@getSouscat');
Route::get('/description/{id}', 'App\Http\Controllers\User\GérerAnonnceController@description');

Route::get('offre', 'App\Http\Controllers\User\OffreController@index');
Route::get('/offre/{id}', 'App\Http\Controllers\User\OffreController@store')->name('user.offre');
Route::post('/offre/{id}', 'App\Http\Controllers\User\OffreController@store')->name('user.offre');
Route::get('listoffre/', 'App\Http\Controllers\User\OffreController@show_list')->name('user.offre.list');
Route::get('/rejeteroffre/{id}', 'App\Http\Controllers\User\OffreController@destroy');

});